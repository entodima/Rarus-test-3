# Задача №3 — книги

<img width="491" alt="Screenshot 2019-07-14 at 22 55 33" src="https://user-images.githubusercontent.com/37329138/61188661-47fdef80-a68b-11e9-92e5-ac8144c08f3a.png">

БД находится в 3НФ: 
 - информация в ячейках является неделимой;
 - неключевый атрибуты полностью зависят от ключевых;
 - нет транзитивных зависимостей отношений.

Запросы выборки находятся в представлениях *show_fantastic_books_and_their_authors* и *show_fantastic_books_and_their_authors* 

<hr>

[Задание](https://github.com/rarus/intern-php-developer/blob/master/task03-books.md)
