# Создание БД, таблиц и внешних ключей
DROP SCHEMA IF EXISTS `rarus` ;
CREATE SCHEMA IF NOT EXISTS `rarus` DEFAULT CHARACTER SET utf8 ;
USE `rarus` ;

DROP TABLE IF EXISTS `rarus`.`authors` ;
CREATE TABLE IF NOT EXISTS `rarus`.`authors` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `rarus`.`books` ;
CREATE TABLE IF NOT EXISTS `rarus`.`books` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `pages` INT(11) NOT NULL,
  `publication_date` DATE NOT NULL,
  `isbn` VARCHAR(17) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `rarus`.`books_authors` ;
CREATE TABLE IF NOT EXISTS `rarus`.`books_authors` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `t2_book_id` INT(11) NOT NULL,
  `author_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `t2_book_id`, `author_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `books_idx` (`t2_book_id` ASC),
  INDEX `author_idx` (`author_id` ASC),
  CONSTRAINT `author`
    FOREIGN KEY (`author_id`)
    REFERENCES `rarus`.`authors` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `book`
    FOREIGN KEY (`t2_book_id`)
    REFERENCES `rarus`.`books` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `rarus`.`categories` ;
CREATE TABLE IF NOT EXISTS `rarus`.`categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `rarus`.`books_categories` ;
CREATE TABLE IF NOT EXISTS `rarus`.`books_categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `t1_book_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `t1_book_id`, `category_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `books_idx` (`t1_book_id` ASC),
  INDEX `categories_idx` (`category_id` ASC),
  CONSTRAINT `books`
    FOREIGN KEY (`t1_book_id`)
    REFERENCES `rarus`.`books` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `categories`
    FOREIGN KEY (`category_id`)
    REFERENCES `rarus`.`categories` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


# Заполнение данных в БД
INSERT INTO `rarus`.`books` (`title`, `pages`, `publication_date`, `isbn`) VALUES 
	('Великий Гэтсби', '180', '2013-05-13', '978-5-699-81496-1'),
	('Ночь нежна', '512', '2013-05-13', '978-5-04-100577-1'),
	("Harry Potter and the Philosopher's Stone. Slytherin Edition", '350', '2017-06-01', '978-1-408-88376-1'),
	('Одноэтажная Америка', '250', '2004-01-13', '5-7516-0630-2'),
	("Цветок пустыни", '320', '2010-01-01', '978-5-9910-1175-4');
		
INSERT INTO `rarus`.`authors` (`first_name`, `last_name`) VALUES 
	('Френсис', 'Фицжеральд'),
    ('Джоан', 'Роулинг'),
    ('Илья', 'Ильф'),
    ('Евгений', 'Петров'),
    ('Уорис', 'Дири'),
    ('Кэтлин', 'Миллер');    
    
INSERT INTO `rarus`.`categories` (`title`) VALUES 
	('Зарубежная классика'),
    ('Классическая проза'),
    ('Литература на английском языке'),
    ('Русская литература'),
    ('Путушествия'),
    ('Фантастика');
    
INSERT INTO books_authors (`t2_book_id`, `author_id`) VALUES 
	((SELECT id from books where title = 'Великий Гэтсби' LIMIT 1), (SELECT id from authors where first_name='Френсис' and last_name='Фицжеральд' LIMIT 1)),
	((SELECT id from books where title = 'Ночь нежна' LIMIT 1), (SELECT id from authors where first_name='Френсис' and last_name='Фицжеральд' LIMIT 1)),
    ((SELECT id from books where title = "Harry Potter and the Philosopher's Stone. Slytherin Edition" LIMIT 1), (SELECT id from authors where first_name='Джоан' and last_name='Роулинг' LIMIT 1)),
	((SELECT id from books where title = 'Одноэтажная Америка' LIMIT 1), (SELECT id from authors where first_name='Илья' and last_name='Ильф' LIMIT 1)),
	((SELECT id from books where title = 'Одноэтажная Америка' LIMIT 1), (SELECT id from authors where first_name='Евгений' and last_name='Петров' LIMIT 1)),
    ((SELECT id from books where title = 'Цветок пустыни' LIMIT 1), (SELECT id from authors where first_name='Уорис' and last_name='Дири' LIMIT 1)),
    ((SELECT id from books where title = 'Цветок пустыни' LIMIT 1), (SELECT id from authors where first_name='Кэтлин' and last_name='Миллер' LIMIT 1));
    
INSERT INTO books_categories (`t1_book_id`, `category_id`) values
	((SELECT id from books where title = 'Великий Гэтсби' LIMIT 1),(select id from categories where title = 'Зарубежная классика')),
    ((select id from books where title = 'Великий Гэтсби' LIMIT 1),(select id from categories where title = 'Классическая проза')),
	((SELECT id from books where title = 'Ночь нежна' LIMIT 1),(select id from categories where title = 'Зарубежная классика')),
    ((select id from books where title = 'Ночь нежна' LIMIT 1),(select id from categories where title = 'Классическая проза')),
    ((select id from books where title = "Harry Potter and the Philosopher's Stone. Slytherin Edition" LIMIT 1),(select id from categories where title = 'Литература на английском языке')),
    ((select id from books where title = "Harry Potter and the Philosopher's Stone. Slytherin Edition" LIMIT 1),(select id from categories where title = 'Фантастика')),
    ((select id from books where title = 'Одноэтажная Америка' LIMIT 1),(select id from categories where title = 'Русская литература')),
    ((select id from books where title = 'Одноэтажная Америка' LIMIT 1),(select id from categories where title = 'Путушествия')),
    ((select id from books where title = "Цветок пустыни" LIMIT 1),(select id from categories where title = 'Фантастика'));
    

# Создание представлений: название книги и ее авторов для жанра “Фантастика”; автор, который написал больше всего книг,
CREATE VIEW `show_fantastic_books_and_their_authors` AS
SELECT books.title as 'Название книги', GROUP_CONCAT(authors.first_name,' ', authors.last_name) as 'Автор(ы)' FROM rarus.books 
join books_categories on books_categories.t1_book_id = books.id 
join categories on categories.id = books_categories.category_id and categories.title = 'Фантастика'
join books_authors on books_authors.t2_book_id = books.id
join authors on authors.id = books_authors.author_id group by books.title;

CREATE VIEW `show_the_most_author_and_his_books_count` AS
SELECT CONCAT(authors.first_name, ' ', authors.last_name) as 'Автор', count(*) as 'Книг'
FROM rarus.books_authors
join authors on authors.id = books_authors.author_id 
group by author_id order by count(*) desc limit 1;